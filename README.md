# HA Router

This project builds and configures 2 basic, highly available routers on Raspberry Pi 4's, using Ubuntu Server as the base, 
iptables for the firewall and networking, ucarp for "High Availability" and lastly, cloud-init and bash for automation.

I use cloud-init to automate the building of the server OS and to configure it as a router. I also created bash scripts to configure the cloud-init files for the user and to automate the easy burning of the Raspberry Pi Image to an SD Card.

### Hardware Requirements

This project was built using 2 (2GB)Raspberry Pi 4's, each with ethernet expansion HAT's to serve as a secondary interface (LAN port) and a simple 5 port ethernet switch to allow the routers to both have a simultaneous connection to an ISP Modem.  

* [Ethernet HAT](https://www.amazon.com/Ethernet-USB-HUB-HAT-Compatible/dp/B07T16RSFM)
  * You can also use an ethernet usb dongle

* [Raspberry Pi (2GB)](https://www.pishop.us/product/raspberry-pi-4-model-b-2gb/?src=raspberrypi)

* [TP-Link Litewave 5 Port Gigabit Ethernet Switch](https://www.tp-link.com/us/home-networking/5-port-switch/ls1005g/)

For the Ethernet HAT and switch, please consult the manufacturers documentation for installation instructions and troubleshooting.  Below is a high-level diagram of how things are physically "networked"

 ![Router_Diagram](./Router_Diagram.png)

You can use any Raspberry Pi model you want or any device you can install Linux on for that matter however, this instruction will only cover using cloud-init with a Raspberry Pi Ubuntu Server image.  You can also adapt this set-up to use a Raspberry Pi's wifi antenna as a secondary interface (not included in this instruction - requires wpa supplicant config).  As you know, the possibilities are endless and this instruction only serves as a foundation for any setup.  

## Preliminary Steps

The only thing I haven't been able to automate is figuring out what the name of the secondary interface on your Raspberry Pi is.  Whether you use the Ethernet HAT listed in the "Hardware Requirements" above or a usb ethernet dongle, that name is always unique.  The only way I know is from with-in the OS itself.  

If you don't already know how to burn a Raspberry Pi image to an SD card, please consult [this documentation](https://www.raspberrypi.com/documentation/computers/getting-started.html).  Once you've booted up your Pi and are logged in, run the following command and PLEASE accurately note the output.  When you run the script to configure your cloud-init file, you will be prompted for this information and if it's wrong, it will mess up the entire build. 

```ip a show | grep -vP 'lo|eth0|wlan' | grep '[[:digit:]]\: ' | awk '{print $2}' | sed 's/://'```

NOTE:  You must capture and have handy this secondary interface name for each router you want to build.

This project was automated to expect your network to follow a Class C Scheme (i.e. 192.168.1.0/24 or 192.168.100.0/24, etc, etc).  Secondly, DHCP is also configured expecting this same scheme and will lease out IP's starting at the 4th IP address in that pool (i.e. 192.168.1.4 or 192.168.100.4, etc, etc - Next section will help explain why)  This pool can be changed after your routers are built and up. (DNS and DHCP section for more on that.)

Once you have the names for your Pi's secondary interfaces, then for each router run:

```
./configure_cloud_init
```
and then to burn the image to the SD Card (one for each router) run:

```
./build_router -n <name of router>
```

For an in-depth explanation of how these routers get built, please read further.  

## High Availability And A Virtual IP (VIP)

"High Availability" is a term used to describe a service or "thing" that is resilient to failure.  In this case our router is the service or "thing" that will be resilliant, or more specifically it's role as a "default gateway."  As you know from "networking 101" a router's IP address is what devices use as their default gateway when configuring or being configured with ip address information.  

Here is where ucarp comes into play and what is automatically configured to make this router "Highly Available".  Ucarp is a simple program that essentially allows you to make an IP address become "virtual" (VIP) and as such be "shared" between two or more machines.  There fore if one machine goes down, another machine will assume that "VIP" and all other devices on the network are none the wiser.  To them, that IP (VIP) is always up which in our case is a routers 192.168.1.1 address (or whatever ip scheme you choose - important NOTE on this below.)  The device that assumes the VIP is called "Master" and all other devices are known as "Backups" (you can have multiple nodes in a ucarp set-up for High Availability, although for this project there are only 2.)

In addition to that VIP that your routers share and is implemented by the ucarp program (i.e. - 192.168.1.1), they each also will have a "Real" ip address that is unique and are used to allow them to communicate on the network.  (i.e. - 192.168.1.2 for router1 and 192.168.1.3 for router2)  Remember the VIP (default gateway) only exists on one router and when that router goes down, that VIP is assumed by the other router.  These "Real" ips allow them to communicate on the network when the other router has the VIP.  Below is a diagram to help you visualize the concept of a "Virtual" IP and a "Real" IP.

![Ucarp_Diagram](./Ucarp_Diagram.png)

 
## Building The Routers

Like mentioned earlier, there exist two bash scripts that build your router. The first, configures your cloud-init files and the second one burns your image to the Raspberry Pi with the configured cloud-init files copied over.

#### Cloud-Init

The "configure\_cloud_init" script will prompt you for all the data that will be used to configure your router.  The script will prompt you with instructions and a brief explanation of each piece of data being requested but please place special importance to these:

- vip
  - This would be the default gateway of your network and the "virtual IP" that your two routers will share between themselves.
- ip
  - This would be the actual IP of your router which is unique.  It's suggested that these be the 2nd and 3rd IP's in your range (i.e. - 192.168.100.2 and 192.168.100.3).  It's important to know which IP is for the router you're building with the configuration script.
- interface
  - This is the secondary interface name for your Pi (explained in the "Preliminary Steps" section above) and it's also important to know which name is which for the router you're building with the configuration and build scripts.  

When you've finished running the "configure\_cloud_init" script, you will have these files in the directory from which you ran the script from:

- cloud-init *.cfg file (named after the router name you chose i.e. - router1cloud.cfg)
- cloud-init network-config.cfg file (also named after the router name you chose i.e. - router1network-config.cfg)
- public and private key pairs to ssh into the router you configured (yup you guessed it, also named after the router name you chose i.e. - router1 and router1.pub)

These cloud-init files will be used by the "build\_router" script to burn the Raspberry Pi image to your SD card (explained below.)  Once these cloud-init files are created, you can use them going forward to "re-build" your routers (although any post configuration changes you make will not exist in them.)  

Run the "configure\_cloud_init" script for each router you want to build like so:

```./configure_cloud_init```
  
#### Burning The Image

Once you've configured your cloud-init files, you can run the "build\_router" script to burn the Raspberry Pi image to your SD Card with the cloud-init files included in the image.  This script must be run as sudo for the mounting and unmounting of the SD card in addition for burning the image using the ```dd``` command.

You can run the script with the ```-h``` option to get usage info but the script only has two options with arguments.  The first is optional and is the path to the Raspberry Pi image (if omitted the script will prompt you for it's path) and the second is required (and important) and is the name of the router you gave to the "configure\_cloud_init" script and for which you are currently buring the image for.  

You run this script like so:

```sudo ./build_router -f /path/to/.img -n router_name```

Once the script finishes running, you pop-out the SD card, pop-it into your Rasberry Pi and boot it up. The complete building of your router will take approximately 18 minutes before it's finished (with two reboots inbetween).  There is a part in the middle where you're presented with a login screen and it seems like it's done but if you wait a minute, you'll see cloud-init continue to run it's automations.  

## DNS and DHCP

DNS and DHCP are facilitated by the installation of pi-hole which is automated via cloud-init.  You can access the web GUI for pi-hole by either going to the VIP or Real IP address of the router that is currently "Master." i.e. - https://192.168.1.1/admin

By default, the password for the GUI is "password."  You can change this from with-in the GUI or on the command line.

Please reference [pi-hole's documenation](https://docs.pi-hole.net/) for for information on this and other configurations and settings.

Please consider donating to the pi-hole project [here](https://pi-hole.net/donate/#donate).  Once you experience their awesome ad-blocking capabilities, you won't go back!


## Logging In With SSH

Everytime you run the "configure\_cloud_init" script to configure a new router, a pair of ssh keys are created and "baked" in when they're built.  This means you can use them to securely remote into your your router as soon as they're up (assuming you're on it's network), you don't even have to copy them over in any way.  Just run run the below command to ssh in:

```ssh -i /path/to/key -o identitiesonly=true <username>@<ip address>```

## Troubleshooting

Although I've done extensive testing to ensure the automations work and stand up working HA Routers, I know users will still encounter problems I didn't discover through my trials.  For those, please submit and issue and I will work to resolve those asap.

In the mean-time if you're having trouble, just re-run the automation scripts and take it slowsly, ensuring you're entering in the right data and that the information is appropriate for the router you're building (router 1 or router 2.)  

## Sources

When I first started my "open-source" journey, I would often hear the term "standing on the shoulders of giants."  My path towards creating this project was NO exception.  Below are the resources I used to learn the technolotgies I built this project with

- Cloud-Init
  - [Cloud-Init Documentation](https://cloudinit.readthedocs.io/en/latest/)
  - [Learn Linux TV](https://youtu.be/exeuvgPxd-E)
- Iptables
  - [O'Reilly Linux iptables Pocket Reference](https://www.oreilly.com/library/view/linux-iptables-pocket/9780596801861/)
  - https://ixnfo.com/en/iptables-rules-for-dhcp.html
  - https://www.debuntu.org/how-to-redirecting-network-traffic-to-a-new-ip-using-iptables/
  - https://youtu.be/XKfhOQWrUVw
- Ucarp
  - [Ucarp Creators github - Frank Denis](https://github.com/jedisct1/UCarp)
  - [OpenBSD Router with fail-over using CARP](https://youtu.be/gT3sKARj7tg)
  - [Rapscallion Blog](https://rascaldev.io/2018/03/13/vip-management-with-ucarp/)
  - https://tweenpath.net/simple-failover-cluster-using-ucarp-on-ubuntu-heartbeat-alternative/
  - https://man.archlinux.org/man/ucarp.8.en
  -

